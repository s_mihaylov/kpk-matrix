﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolProject
{
    public class Student
    {
        private string name;
        private int uniqueNumber;

        /// <summary>
        /// Gets or sets Student's name.
        /// </summary>
        /// <value>Gets or sets the value of the name field.</value>
        /// <exception cref="System.ArgumentException">Thrown when
        /// the value to set is null, empty or contains only
        /// whitespace characters.</exception>
        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    {
                        throw new ArgumentException("You must enter at least one charackter for student name.", "value  ");
                    }
                }
                else                {

                    name = value;
                }
            }
        }
        public int UniqueNumber
        {
            get { return uniqueNumber; }
            private set
            {
                if (value < 10000 || value > 99999)
                {
                    throw new ArgumentOutOfRangeException("You must enter unique number between 10000 and 99999.");
                }
                else
                {
                    uniqueNumber = value;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Student"/> class.
        /// </summary>
        /// <param name="name">The name of the student.</param>
        /// <param name="uniqueNumber">The uniqueNumber of the student.</param>
        /// <exception cref="System.ArgumentException">Thrown when
        /// <paramref name="name"/> is null, empty or contains only
        /// whitespace characters.</exception>
        public Student(string name, School school)
        {
            this.Name = name;
            this.UniqueNumber = school.GetUniqueNumber();
        }

        /// <summary>
        /// Returns the string representation of the student.
        /// </summary>
        /// <returns>A string containing Student's name and Id.</returns>
        public override string ToString()
        {
            return String.Format("{0} with unique number {1}.", Name, UniqueNumber);
        }
        
    }
}
