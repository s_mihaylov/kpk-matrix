﻿using System;
using System.Globalization;

namespace Methods
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherInfo { get; set; }

        public bool IsOlderThan(Student other)
        {
            string[] stringFormat = new string[] { "d.M.yyyy", "dd.MM.yyyy", "d,M,yyyy", "dd,MM,yyyy" };
            DateTime firstDate = DateTime.ParseExact(this.OtherInfo.Substring(this.OtherInfo.Length - 10), stringFormat, CultureInfo.InvariantCulture, DateTimeStyles.None);
            DateTime secondDate = DateTime.ParseExact(other.OtherInfo.Substring(other.OtherInfo.Length - 10), stringFormat, CultureInfo.InvariantCulture, DateTimeStyles.None);
            return firstDate > secondDate;
        }
    }
}
