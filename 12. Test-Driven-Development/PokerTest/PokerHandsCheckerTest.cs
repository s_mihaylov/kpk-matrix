﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker;
using System.Collections.Generic;

namespace PokerTest
{
    [TestClass]
    public class PokerHandsCheckerTest
    {
        private static PokerHandsChecker pokerHandsChecker;

        [ClassInitialize]
        public static void InitPokerHandsChecker(TestContext context)
        {
           pokerHandsChecker = new PokerHandsChecker();
        }
        [TestMethod]
        public void TestIsValid1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Two, CardSuit.Hearts),
                new Card(CardFace.Jack, CardSuit.Clubs),
                new Card(CardFace.Two, CardSuit.Hearts),
                new Card(CardFace.Five, CardSuit.Diamonds)
            };

            IHand hand = new Hand(cards);

            bool validHand = pokerHandsChecker.IsValidHand(hand);

            Assert.IsFalse(validHand, "Hand validation does not work correctly (accepts identical cards).");
        }

        [TestMethod]
        public void TestIsValid2()
        {
            bool validHand = pokerHandsChecker.IsValidHand(null);

            Assert.IsFalse(validHand, "Hand validation does not work correctly.");
        }

        [TestMethod]
        public void TestIsValid3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Two, CardSuit.Hearts),
                new Card(CardFace.Jack, CardSuit.Clubs)
            };

            IHand hand = new Hand(cards);

            bool validHand = pokerHandsChecker.IsValidHand(hand);

            Assert.IsFalse(validHand, "Hand validation does not work correctly.");
        }

        [TestMethod]
        public void TestStraight1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Queen, CardSuit.Clubs),
                new Card(CardFace.Jack, CardSuit.Hearts),
                new Card(CardFace.Ten, CardSuit.Spades),
                new Card(CardFace.Nine, CardSuit.Hearts),
                new Card(CardFace.Eight, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);
            
            Assert.IsTrue(pokerHandsChecker.IsStraight(hand), "Straight is not identified correctly.");
        }

        [TestMethod]
        public void TestStraight2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ten, CardSuit.Clubs),
                new Card(CardFace.Nine, CardSuit.Diamonds),
                new Card(CardFace.Eight, CardSuit.Hearts),
                new Card(CardFace.Seven, CardSuit.Clubs),
                new Card(CardFace.Six, CardSuit.Spades)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsStraight(hand), "Straight is not identified correctly.");
        }

        [TestMethod]
        public void TestStraight3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ace, CardSuit.Clubs),
                new Card(CardFace.King, CardSuit.Clubs),
                new Card(CardFace.Queen, CardSuit.Diamonds),
                new Card(CardFace.Jack, CardSuit.Spades),
                new Card(CardFace.Ten, CardSuit.Spades)
            };
            IHand hand = new Hand(cards);
            
            Assert.IsTrue(pokerHandsChecker.IsStraight(hand), "Straight is not identified correctly.");
        }

        [TestMethod]
        public void TestStraight4()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Five, CardSuit.Spades),
                new Card(CardFace.Four, CardSuit.Diamonds),
                new Card(CardFace.Three, CardSuit.Diamonds),
                new Card(CardFace.Two, CardSuit.Hearts),
                new Card(CardFace.Ace, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsStraight(hand), "Straight is not identified correctly.");
        }

        [TestMethod]
        public void TestNotStraight()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ten, CardSuit.Spades),
                new Card(CardFace.Two, CardSuit.Diamonds),
                new Card(CardFace.Ace, CardSuit.Hearts),
                new Card(CardFace.King, CardSuit.Spades),
                new Card(CardFace.Queen, CardSuit.Clubs)
            };
            IHand hand = new Hand(cards);

            Assert.IsFalse(pokerHandsChecker.IsStraight(hand), "High card is not identified correctly.");
        }

        [TestMethod]
        public void TestFlush1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Queen, CardSuit.Clubs),
                new Card(CardFace.Ten, CardSuit.Clubs),
                new Card(CardFace.Seven, CardSuit.Clubs),
                new Card(CardFace.Six, CardSuit.Clubs),
                new Card(CardFace.Four, CardSuit.Clubs)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFlush(hand), "Flush is not identified correctly.");
        }

        [TestMethod]
        public void TestFlush2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Queen, CardSuit.Diamonds),
                new Card(CardFace.Nine, CardSuit.Diamonds),
                new Card(CardFace.Seven, CardSuit.Diamonds),
                new Card(CardFace.Four, CardSuit.Diamonds),
                new Card(CardFace.Three, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFlush(hand), "Flush is not identified correctly.");
        }

        [TestMethod]
        public void TestFlush3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.King, CardSuit.Hearts),
                new Card(CardFace.Queen, CardSuit.Hearts),
                new Card(CardFace.Nine, CardSuit.Hearts),
                new Card(CardFace.Five, CardSuit.Hearts),
                new Card(CardFace.Four, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFlush(hand), "Flush is not identified correctly.");
        }

        [TestMethod]
        public void TestStraightFlush1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Queen, CardSuit.Clubs),
                new Card(CardFace.Jack, CardSuit.Clubs),
                new Card(CardFace.Ten, CardSuit.Clubs),
                new Card(CardFace.Nine, CardSuit.Clubs),
                new Card(CardFace.Eight, CardSuit.Clubs)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsStraightFlush(hand), "Straight flush is not identified correctly.");
        }

        [TestMethod]
        public void TestStraightFlush2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Five, CardSuit.Diamonds),
                new Card(CardFace.Four, CardSuit.Diamonds),
                new Card(CardFace.Three, CardSuit.Diamonds),
                new Card(CardFace.Two, CardSuit.Diamonds),
                new Card(CardFace.Ace, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsStraightFlush(hand), "Straight flush is not identified correctly.");
        }

        [TestMethod]
        public void TestStraightFlush3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ace, CardSuit.Diamonds),
                new Card(CardFace.King, CardSuit.Diamonds),
                new Card(CardFace.Queen, CardSuit.Diamonds),
                new Card(CardFace.Jack, CardSuit.Diamonds),
                new Card(CardFace.Ten, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsStraightFlush(hand), "Straight flush is not identified correctly.");
        }

        [TestMethod]
        public void TestFourOfAKind1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Nine, CardSuit.Diamonds),
                new Card(CardFace.Nine, CardSuit.Clubs),
                new Card(CardFace.Nine, CardSuit.Hearts),
                new Card(CardFace.Jack, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFourOfAKind(hand), "Four of a kind is not identified correctly.");
        }

        [TestMethod]
        public void TestFourOfAKind2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Seven, CardSuit.Diamonds),
                new Card(CardFace.Seven, CardSuit.Spades),
                new Card(CardFace.Seven, CardSuit.Clubs),
                new Card(CardFace.Seven, CardSuit.Hearts),
                new Card(CardFace.Ten, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFourOfAKind(hand), "Four of a kind is not identified correctly.");
        }

        [TestMethod]
        public void TestFourOfAKind3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.King, CardSuit.Clubs),
                new Card(CardFace.King, CardSuit.Spades),
                new Card(CardFace.King, CardSuit.Hearts),
                new Card(CardFace.Jack, CardSuit.Diamonds),
                new Card(CardFace.King, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);
            
            Assert.IsTrue(pokerHandsChecker.IsFourOfAKind(hand), "Four of a kind is not identified correctly.");
        }

        [TestMethod]
        public void TestFullHouse1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Three, CardSuit.Clubs),
                new Card(CardFace.Three, CardSuit.Spades),
                new Card(CardFace.Three, CardSuit.Hearts),
                new Card(CardFace.Six, CardSuit.Diamonds),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFullHouse(hand), "Full house is not identified correctly.");
        }

        [TestMethod]
        public void TestFullHouse2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Seven, CardSuit.Spades),
                new Card(CardFace.Seven, CardSuit.Diamonds),
                new Card(CardFace.Seven, CardSuit.Hearts),
                new Card(CardFace.Four, CardSuit.Diamonds),
                new Card(CardFace.Four, CardSuit.Spades)
            };
            IHand hand = new Hand(cards);
            
            Assert.IsTrue(pokerHandsChecker.IsFullHouse(hand), "Full house is not identified correctly.");
        }

        [TestMethod]
        public void TestFullHouse3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Six, CardSuit.Clubs),
                new Card(CardFace.Ace, CardSuit.Spades),
                new Card(CardFace.Ace, CardSuit.Clubs),
                new Card(CardFace.Six, CardSuit.Diamonds),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFullHouse(hand), "Full house is not identified correctly.");
        }

        [TestMethod]
        public void TestFullHouse4()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Queen , CardSuit.Clubs),
                new Card(CardFace.Queen, CardSuit.Spades),
                new Card(CardFace.Queen, CardSuit.Diamonds),
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Nine, CardSuit.Clubs)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsFullHouse(hand), "Full house is not identified correctly.");
        }

        [TestMethod]
        public void TestOnePair1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Four, CardSuit.Spades),
                new Card(CardFace.Four, CardSuit.Diamonds),
                new Card(CardFace.King, CardSuit.Hearts),
                new Card(CardFace.Ten, CardSuit.Spades),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);
        
            Assert.IsTrue(pokerHandsChecker.IsOnePair(hand), "One pair is not identified correctly.");
        }

        [TestMethod]
        public void TestOnePair2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Two, CardSuit.Clubs),
                new Card(CardFace.Ten, CardSuit.Spades),
                new Card(CardFace.King, CardSuit.Hearts),
                new Card(CardFace.Ten, CardSuit.Diamonds),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsOnePair(hand), "One pair is not identified correctly.");
        }

        [TestMethod]
        public void TestTwoPair1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Jack, CardSuit.Clubs),
                new Card(CardFace.Jack, CardSuit.Diamonds),
                new Card(CardFace.Four, CardSuit.Hearts),
                new Card(CardFace.Four, CardSuit.Spades),
                new Card(CardFace.Six, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsTwoPair(hand), "Two pair is not identified correctly.");
        }

        [TestMethod]
        public void TestTwoPair2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ten, CardSuit.Clubs),
                new Card(CardFace.Ten, CardSuit.Diamonds),
                new Card(CardFace.Seven, CardSuit.Spades),
                new Card(CardFace.Seven, CardSuit.Hearts),
                new Card(CardFace.Two, CardSuit.Diamonds)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsTwoPair(hand), "Two pair is not identified correctly.");
        }
                
        [TestMethod]
        public void TestTwoPair3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Ten, CardSuit.Clubs),
                new Card(CardFace.Ten, CardSuit.Spades),
                new Card(CardFace.Five, CardSuit.Hearts),
                new Card(CardFace.Five, CardSuit.Diamonds),
                new Card(CardFace.Ace, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsTwoPair(hand), "Two pair is not identified correctly.");
        }

        [TestMethod]
        public void TestTwoPair4()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Three, CardSuit.Clubs),
                new Card(CardFace.King, CardSuit.Spades),
                new Card(CardFace.King, CardSuit.Diamonds),
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Nine, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsTwoPair(hand), "Two pair is not identified correctly.");
        }
        
        [TestMethod]
        public void TestThreeOfAKind1()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Jack, CardSuit.Clubs),
                new Card(CardFace.Jack, CardSuit.Diamonds),
                new Card(CardFace.Jack, CardSuit.Hearts),
                new Card(CardFace.Ace, CardSuit.Diamonds),
                new Card(CardFace.Queen, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsThreeOfAKind(hand), "Three of a kind is not identified correctly.");
        }

        [TestMethod]
        public void TestThreeOfAKind2()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Three, CardSuit.Clubs),
                new Card(CardFace.Three, CardSuit.Diamonds),
                new Card(CardFace.Three, CardSuit.Hearts),
                new Card(CardFace.Nine, CardSuit.Spades),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsThreeOfAKind(hand), "Three of a kind is not identified correctly.");
        }

        [TestMethod]
        public void TestThreeOfAKind3()
        {
            IList<ICard> cards = new List<ICard>()
            {
                new Card(CardFace.Six, CardSuit.Spades),
                new Card(CardFace.Eight, CardSuit.Spades),
                new Card(CardFace.Seven, CardSuit.Hearts),
                new Card(CardFace.Six, CardSuit.Diamonds),
                new Card(CardFace.Six, CardSuit.Hearts)
            };
            IHand hand = new Hand(cards);

            Assert.IsTrue(pokerHandsChecker.IsThreeOfAKind(hand), "Three of a kind is not identified correctly.");
        }
    }
}
