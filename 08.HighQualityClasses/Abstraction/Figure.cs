﻿using System;

namespace RefactoringAbstraction
{
    abstract class Figure: IFigure
    {
        
        public abstract double CalcPerimeter();

        public abstract double CalcSurface();
    }
}
