﻿using System;

namespace Poker
{
    public class Card : ICard
    {
        private readonly char[] suits = { '♣', '♦', '♥', '♠' };
        private readonly string[] faces = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
        public CardFace Face { get; private set; } 
        public CardSuit Suit { get; private set; } 
                                                   
        // <summary>
        /// Initializes a new instance of the <see cref="Card"/> class.
        /// </summary>
        /// <param name="face">Card face.</param>
        /// <param name="suit">Card suit.</param>
        public Card(CardFace face, CardSuit suit)
        {
            this.Face = face;
            this.Suit = suit;
        }

        public override string ToString()
        {
            return faces[(int)this.Face - 2] + suits[(int)this.Suit];
        }

        /// <summary>
        /// Compares two <see cref="ICard"/> instances.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>Less than zero if this instance is of lower face,
        /// zero if both cards are of the same face, greater than zero if
        /// this instance is of higher face.</returns>
        public int CompareTo(ICard other)
        {
            return this.Face.CompareTo(other.Face);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another
        /// <see cref="ICard"/> object.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>True if objects are equal, otherwise - false.</returns>
        public override bool Equals(object obj)
        {
            ICard other = (ICard)obj;
            return this.Face == other.Face && this.Suit == other.Suit;
        }
    }
}
